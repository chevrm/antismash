# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2012 Daniyal Kazempour
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# Copyright (C) 2012,2013 Kai Blin
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
'''
More detailed lassopeptide analysis using HMMer-based leader peptide
cleavage sites prediction as well as prediction of number of  dissulfide
bridges, molcular mass and macrolactam ring.
'''

import logging
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from antismash import utils


class Lassopeptide(object):
    '''
    Class to calculate and store lassopeptide information
    '''
    def __init__(self, start, end, score):
        self.start = start
        self.end = end
        self.score = score
        self._leader = ''
        self._lassotype = 'Class-II'
        self._core = ''
        self._weight = -1
        self._monoisotopic_weight = -1
        self._num_bridges = 0
        self._macrolactam = ''
        self._c_cut = ''


    @property
    def core(self):
        return self._core

    @core.setter
    def core(self, seq):
        seq = seq.replace('X', '')
        self.core_analysis_monoisotopic = ProteinAnalysis(seq, monoisotopic=True)
        self.core_analysis = ProteinAnalysis(seq, monoisotopic=False)
        self._core = seq

    @property
    def leader(self):
        return self._leader

    @leader.setter
    def leader(self, seq):
        self._leader = seq

    @property
    def c_cut(self):
        return self._c_cut

    @c_cut.setter
    def c_cut(self, ccut):
        self._c_cut = ccut

    def __repr__(self):
        return "Lassopeptide(%s..%s, %s, %r, %r, %s, %s(%s), %s, %s)" % (self.start, self.end, self.score, self._lassotype, self._core, self._num_bridges, self._monoisotopic_weight, self._weight, self._macrolactam, self.c_cut)


    def _calculate_mw(self):
        '''
        (re)calculate the monoisotopic mass and molecular weight
        '''
        if not self._core:
            raise ValueError()

        aas = self.core_analysis.count_amino_acids()      
        mol_mass = self.core_analysis.molecular_weight()
            
        CC_mass = 0
        if self._num_bridges != 0:
             CC_mass = 2*self._num_bridges

        # dehydration indicative of cyclization     
        bond = 18.02
        self._weight = mol_mass + CC_mass - bond


        monoisotopic_mass = self.core_analysis_monoisotopic.molecular_weight()
        self._monoisotopic_weight = monoisotopic_mass + CC_mass - bond

        
    @property
    def monoisotopic_mass(self):
        '''
        function determines the weight of the core peptide
        '''
        if self._monoisotopic_weight > -1:
            return self._monoisotopic_weight

        self._calculate_mw()
        return self._monoisotopic_weight

    
    @property
    def molecular_weight(self):
        '''
        function determines the weight of the core peptide
        '''
        if self._weight > -1:
            return self._weight

        self._calculate_mw()
        return self._weight


    @property 
    def macrolactam(self):
        '''
        Predict the lassopeptide macrolactam ring
        '''
        
        if not self._core:
            raise ValueError()
        
        seq = self._core[6:9]
        for res in range(len(seq)):
            if seq[res] in ['E', 'D']:
                self._macrolactam = self._core[0:res+7]
                
        return self._macrolactam

    
    @property
    def number_bridges(self):
        '''
        Predict the lassopeptide number of disulfide bridges
        '''

        aas = self.core_analysis.count_amino_acids()
        if aas['C'] >= 2:
            self._num_bridges = 1
        if aas['C'] >= 4:
            self._num_bridges = 2
        return self._num_bridges

    
    @property
    def lasso_class(self):
        '''
        Predict the lassopeptide class based on disulfide bridges
        '''
        if not self._core:
            raise ValueError()

        if self._num_bridges == 1:
             self._lassotype = 'Class-III'
        if self._num_bridges == 2:
             self._lassotype = 'Class-I'
        return self._lassotype
    

def predict_cleavage_site(query_hmmfile, target_sequence):
    '''
    Function extracts from HMMER the start position, end position and score 
    of the HMM alignment
    '''
    hmmer_res = utils.run_hmmpfam2(query_hmmfile, target_sequence)
    resvec = None
    for res in hmmer_res:
        for hits in res:
            lasso_type = hits.description
            for hsp in hits:

                # when hmm includes 1st macrolactam residue: end-2
                resvec = Lassopeptide(hsp.query_start-1, hsp.query_end-1, hsp.bitscore)
                return resvec
    return resvec


thresh_hit = 1.25


def run_lassopred(seq_record, query):
    hmmer_profile = 'precursor_2637.hmm'


    query_sequence = utils.get_aa_sequence(query, to_stop=True)
    lasso_a_fasta = ">%s\n%s" % (utils.get_gene_id(query), query_sequence)
  
    #run sequence against profile and parse them in a vector containing START, END and SCORE
    profile = utils.get_full_path(__file__, hmmer_profile)
    result = predict_cleavage_site(profile, lasso_a_fasta)
    
    
    if result is None:
        logging.debug('%r: No cleavage site predicted' % utils.get_gene_id(query))
        return

    if thresh_hit > result.score:
        logging.debug('%r: Score %0.2f below threshold %0.2f' %
                      (utils.get_gene_id(query), result.score, thresh_hit))
        return

    
    #extract now the core peptide
    result.leader = query_sequence[:result.end]
    result.core = query_sequence[result.end:]
    
    #prediction of cleavage in C-terminal based on lasso's core sequence
    C_term_hmmer_profile = 'tail_cut.hmm'
    thresh_C_hit = -7.5

    aux = result.core[(len(result.core)/2):]
    core_a_fasta = ">%s\n%s" % (utils.get_gene_id(query),aux)
 
    profile_C = utils.get_full_path(__file__, C_term_hmmer_profile)
    hmmer_res_C = utils.run_hmmpfam2(profile_C, core_a_fasta)

    for res in hmmer_res_C:
        for hits in res:
            for seq in hits:
                if seq.bitscore > thresh_C_hit:
                    result.c_cut = aux[seq.query_start+1:]        
                    

    if result is None:
        logging.debug('%r: No C-terminal cleavage site predicted' % utils.get_gene_id(query))
        return


    if not 'sec_met' in query.qualifiers:
        query.qualifiers['sec_met'] = []


    if ";".join(query.qualifiers['sec_met']).find(';Kind: biosynthetic') < 0:
        query.qualifiers['sec_met'].append('Kind: biosynthetic')
        

    return result


def find_features(seq_record, cluster):
    features = []
    for feature in utils.get_cds_features(seq_record):
        if feature.location.start < cluster.location.start or \
           feature.location.end > cluster.location.end:
            continue
        
        features.append(feature)

    return features


def result_vec_to_features(orig_feature, res_vec):
    start = orig_feature.location.start
    end = orig_feature.location.start + (res_vec.end * 3)
    strand = orig_feature.location.strand
    loc = FeatureLocation(start, end, strand=strand)
    leader_feature = SeqFeature(loc, type='CDS_motif')
    leader_feature.qualifiers['note'] = ['leader peptide']
    leader_feature.qualifiers['note'].append('lassopeptide')
    leader_feature.qualifiers['note'].append('predicted leader seq: %s' % res_vec.leader)
    leader_feature.qualifiers['locus_tag'] = [ utils.get_gene_id(orig_feature) ]
    start = end
    end = orig_feature.location.end
    loc = FeatureLocation(start, end, strand=strand)
    core_feature = SeqFeature(loc, type='CDS_motif')
    core_feature.qualifiers['note'] = ['core peptide']
    core_feature.qualifiers['note'].append('lassopeptide')
    core_feature.qualifiers['note'].append('monoisotopic mass: %0.1f' % res_vec.monoisotopic_mass)
    core_feature.qualifiers['note'].append('molecular weight: %0.1f' % res_vec.molecular_weight)
      
    #resets .. to recalculate weights considering C-terminal putative cut
    res_vec._weight = -1
    res_vec._monoisotopic_weight = -1
    oldcore = res_vec.core
    res_vec.core = oldcore[:(len(res_vec.core)-len(res_vec.c_cut))]

    core_feature.qualifiers['note'].append('monoisotopic mass_cut: %0.1f' % res_vec.monoisotopic_mass)
    core_feature.qualifiers['note'].append('molecular weight_cut: %0.1f' % res_vec.molecular_weight)
    
            
    core_feature.qualifiers['note'].append('number of bridges: %s' % res_vec.number_bridges)
    core_feature.qualifiers['note'].append('predicted core seq: %s' % res_vec.core)
    core_feature.qualifiers['note'].append('predicted class: %s' % res_vec.lasso_class)
    core_feature.qualifiers['note'].append('score: %0.2f' % res_vec.score)
    core_feature.qualifiers['note'].append('macrolactam: %s' %res_vec.macrolactam)
    core_feature.qualifiers['note'].append('putative cleaved off residues: %s' %res_vec.c_cut)
    core_feature.qualifiers['locus_tag'] = [ utils.get_gene_id(orig_feature) ]

    return [leader_feature, core_feature]


def specific_analysis(seq_record, options):

    clusters = utils.get_cluster_features(seq_record)
    for cluster in clusters:
        if 'product' not in cluster.qualifiers or \
           'lassopeptide' not in cluster.qualifiers['product'][0]:
            continue
               
        lasso_fs = find_features(seq_record, cluster)
      
        for lasso_f in lasso_fs:
            result_vec = run_lassopred(seq_record, lasso_f)
            if result_vec is None:
                continue
 
            new_features = result_vec_to_features(lasso_f, result_vec)
            seq_record.features.extend(new_features)
          
